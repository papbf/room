package id.ac.ub.room;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    LayoutInflater inflater;
    Context _context;
    List<Item> itemList = new ArrayList<Item>();

    public ItemAdapter(Context _context){
        this._context = _context;
        this.inflater = LayoutInflater.from(this._context);
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }


    @NonNull
    @Override
    public ItemAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.ItemViewHolder holder, int position) {
        Item item = itemList.get(position);
        Log.d(MainActivity.TAG,"data "+position);
        Log.d(MainActivity.TAG,"nim "+item.getNim());
        Log.d(MainActivity.TAG, "nama "+item.getNama());

        holder.tvNim.setText(item.getNim());
        holder.tvNama.setText(item.getNama());

    }

    @Override
    public int getItemCount() {
        Log.d(MainActivity.TAG, "Jumlah data "+this.itemList.size());
        return this.itemList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView tvNim;
        TextView tvNama;

        public ItemViewHolder(@NonNull View itemView){
            super(itemView);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNama = itemView.findViewById(R.id.tvNama);
        }
    }
}
